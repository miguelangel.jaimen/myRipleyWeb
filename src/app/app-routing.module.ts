import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesRoutingModule } from './pages/pages.routing';

import { NotFoundComponent } from './not-found/not-found.component';
import { AuthRoutingModule } from './auth/auth.routing';

const routes: Routes = [

  // path: '/dashboard' PagesRoutingModule
  // path: '/auth' AuthRoutingModule

  {path:'', redirectTo:'/login', pathMatch:'full'},
  {path:'**', component:NotFoundComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    PagesRoutingModule,
    AuthRoutingModule
  ],

  exports: [RouterModule]
})
export class AppRoutingModule { }
