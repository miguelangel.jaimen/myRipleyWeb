import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrl = environment.url;
  constructor(
    private httpClient: HttpClient
  ) { }

  singIn(data: any, id_system: any){
    return this.httpClient.post<any>(`${this.baseUrl}/login/ingreso`, {...data, id_system}).pipe(map(res => res.data));
  }
}
