import { Component, OnInit } from '@angular/core';
import { Login } from './login.model';
import { AuthService } from '../auth.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [
  ]
})
export class LoginComponent implements OnInit {
  login = new Login('','');
  id_system = environment.id_system;
  user:any;
  userLogin = sessionStorage.getItem('user');
  constructor(
    private authService: AuthService,
    private router:Router
  ) { }

  ngOnInit(): void {
    if(this.userLogin) this.router.navigate(['/cliente']);
  }

  singIn(){
    this.authService.singIn(this.login, this.id_system).subscribe( response => {
      this.user = response;
      sessionStorage.setItem('user', JSON.stringify(this.user));
      this.router.navigate(['/cliente']);
    },
    error => {
      Swal.fire({
        title: '<strong><u>Prueba con esto:</u></strong>',
        icon: 'info',
        html:
          'Usuario: <b>cliente@ripley.cl</b></br> ' +
          'Clave: <b>ripley</b> ',
        showCloseButton: false,
        showCancelButton: false,
        focusConfirm: false,
        confirmButtonText:
          '<i class="fa fa-thumbs-up"></i>',
      })
    })
  }

}
