import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AddresseeService {
  baseUrl = environment.url;
  constructor(
    private httpClient: HttpClient
  ) { }

  getBanksList(){
    return this.httpClient.get<any>('https://bast.dev/api/banks.php').pipe(map(rsp => rsp.banks));
  }
  saveAddressee(data: any, id_login: any){
    return this.httpClient.post<any>(`${this.baseUrl}/destinatario/crear`, {...data, id_login}).pipe(map(res => res));
  }
}
