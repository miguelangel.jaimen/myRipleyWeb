export class Addrressee {
    constructor(
        public rut:string,
        public name:string,
        public email:string,
        public phone:string,
        public bank:string,
        public typeAcount:string,
        public numberAcount:string,
    ){}
}