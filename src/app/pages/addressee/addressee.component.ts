import { Component, OnInit } from '@angular/core';
import { Addrressee } from './addressee.model';
import { AddresseeService } from './addressee.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-addressee',
  templateUrl: './addressee.component.html',
  styleUrls: ['./addressee.component.css']
})
export class AddresseeComponent implements OnInit {

  addressee = new Addrressee('','','','','','','');
  typesList = [{"type":"cuenta corriente", "id": "cct"}, {"type":"cuenta vista", "id": "ccv"}, {"type":"cuenta ahorro", "id": "cch"}];
  banksList: any; 
  loginUser:any;
  

  constructor(
    private addresseeService: AddresseeService
  ) { }

  ngOnInit(): void {
    this.getBanks();
    this.loginUser = JSON.parse(sessionStorage.getItem('user') || '{}');
  }

  private getBanks(){
    this.addresseeService.getBanksList().subscribe(res => {
      this.banksList= res;
    })
  };

  saveAddressee(){
    const id_login = this.loginUser.id_login;
    this.addresseeService.saveAddressee(this.addressee, id_login).subscribe( response => {
      Swal.fire('Guardado!', response.data.msjCliente, 'success');
      this.addressee = new Addrressee('','','','','','','');
    },
    error => {
      Swal.fire('Error!', error.msjCliente, 'error');
    })
  }

}
