import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import {environment} from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class TransferService {
  baseUrl = environment.url;
  constructor(
    private httpClient: HttpClient
  ) { }

  listAddressees(id_login: any){
    return this.httpClient.get<any>(`${this.baseUrl}/destinatario/listar/${id_login}`).pipe(map(res => res.data));
  }
  listTransfer(id_login: any){
    return this.httpClient.get<any>(`${this.baseUrl}/transferencias/listar/${id_login}`).pipe(map(res => res.data));
  }
  saveTransfer(data: any, id_login: any){
    return this.httpClient.post<any>(`${this.baseUrl}/transferencias/crear`, {...data, id_login}).pipe(map(res => res.data));
  }


}
