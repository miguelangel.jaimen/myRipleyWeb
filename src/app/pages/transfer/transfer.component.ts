import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { AddresseeService } from '../addressee/addressee.service';
import { Transfer } from './transfer.model';
import {TransferService} from './transfer.service';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.css']
})
export class TransferComponent implements OnInit {

  addresseeList:any;
  transfer = new Transfer('','',0);
  loginUser:any;
  addresseeSelect:any;
  banksList: any;
  banco: any;
  typesList = [{"type":"cuenta corriente", "id": "cct"}, {"type":"cuenta vista", "id": "ccv"}, {"type":"cuenta ahorro", "id": "cch"}];
  constructor(
    private addresseeService: AddresseeService,
    private transferService: TransferService
  ) { }

  ngOnInit(): void {
    this.loginUser = JSON.parse(sessionStorage.getItem('user') || '{}');
    this.listAddressees();
    this.getBanks();
  }

  listAddressees(){
    let id_login = this.loginUser.id_login;
    this.transferService.listAddressees(id_login).subscribe(res => {
      this.addresseeList= res;
    })
  }

  filterOption(id:any){
    this.addresseeSelect = this.addresseeList.filter((a :any)=> a.id_addressee === id)[0];

    if(this.addresseeSelect && this.addresseeSelect.bank){
      this.banco = this.banksList.filter((b :any)=> b.id === this.addresseeSelect.bank)[0];
      this.addresseeSelect.tipocuenta = this.typesList.find((c :any) => c.id === this.addresseeSelect.typeAcount)?.type
    }
  }

  private getBanks(){
    this.addresseeService.getBanksList().subscribe(res => {
      this.banksList= res;
    })
  };

  saveTransfer(){
    const id_login = this.loginUser.id_login;
    this.transferService.saveTransfer(this.transfer, id_login).subscribe( response => {
      Swal.fire('Hecho!', response.msjCliente, 'success');
      this.transfer = new Transfer('','',0);
      this.addresseeSelect = null;
    },
    error => {
      Swal.fire('Error!', error.error.msjCliente, 'error');
    })
  }
}
