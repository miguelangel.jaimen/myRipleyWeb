import { Component, OnInit } from '@angular/core';
import { promise } from 'selenium-webdriver';
import { AddresseeService } from '../addressee/addressee.service';
import { TransferService } from '../transfer/transfer.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
 
  loginUser: any;
  transferList: any;
  banksList: any;
  constructor(
    private transferService : TransferService,
    private addresseeService : AddresseeService
  ) { }

  ngOnInit(): void {
   // this.getBanks();
    this.loginUser = JSON.parse(sessionStorage.getItem('user') || '{}');
    this.listTransfers();
  }

  listTransfers(){
    let id_login = this.loginUser.id_login;
    this.transferService.listTransfer(id_login).subscribe(res => {
      this.transferList = res;
    });
  }

  private async getBanks(){
    this.addresseeService.getBanksList().subscribe(res => {
      this.getBank(res);
    })
  };

  getBank(list:any){
      this.transferList.map((x :any)=>{           
       x.destinatario.banco = list? list.find((e: any) => e.id === x.destinatario.bank).name: x.destinatario.bank;
      });
  }

}
