import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddresseeComponent } from './addressee/addressee.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HistoryComponent } from './history/history.component';
import { PagesComponent } from './pages.component';
import { TransferComponent } from './transfer/transfer.component';


export const routes: Routes = [
    {
        path: 'cliente',
        component: PagesComponent,
        children:[
            {path:'',component:DashboardComponent},
            {path:'transferencia', component:TransferComponent},
            {path:'destinatario', component:AddresseeComponent},
            {path:'historicas', component:HistoryComponent},
        ]
    }
];

@NgModule({
    imports:[ RouterModule.forChild(routes)],
    exports: [ RouterModule]
})

export class PagesRoutingModule {}