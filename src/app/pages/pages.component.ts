import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styles: [
  ]
})
export class PagesComponent implements OnInit {
  
  route: any;
  description: any;
  userLogin = sessionStorage.getItem('user');

  constructor(
    private router: Router
    ) { }
    
    ngOnInit(): void {
    if(!this.userLogin) this.router.navigate(['/login']);
    this.route = this.router.url.replace('/', '');
    this.description = 'Formulario para la creación de un nuevo destinatario'
  }

}
