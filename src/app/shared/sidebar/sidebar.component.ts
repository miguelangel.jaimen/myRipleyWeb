import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styles: [
  ]
})
export class SidebarComponent implements OnInit {

  loginUser:any;
  constructor(
    private router: Router
  ) { }

  ngOnInit(
  ): void {
    this.getLoginUser();
  }

  getLoginUser(){
    
    this.loginUser = JSON.parse(sessionStorage.getItem('user') || '{}');
  }

  logOut(){
    sessionStorage.removeItem('user');
    this.router.navigate(['/login']);
  }

}
